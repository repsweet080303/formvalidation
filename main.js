//contrustor function
function Validator(options) {
  function getParent(element, selector) {
    while (element.parentElement) {
      if (element.parentElement.matches(selector)) {
        return element.parentElement;
      }
      element = element.parentElement;
    }
  }
  var selectorRule = {};
  //function execute validate
  function validate(inputElement, rule) {
    var errorElement = getParent(
      inputElement,
      options.formGroupSelector
    ).querySelector(options.errorSelector);
    var errorMessage;
    var rules = selectorRule[rule.selector];
    //loof rule of selector
    //if have error message then out of loof
    for (var i = 0; i < rules.length; i++) {
      switch (inputElement.type) {
        case 'radio':
        case 'checkbox':
          errorMessage = rules[i](
            formElement.querySelector(rule.selector + ':checked')
          );
          break;
        default:
          errorMessage = rules[i](inputElement.value);
      }
      if (errorMessage) break;
    }
    //rule.selector get rule of selector
    // console.log(rules);
    if (errorMessage) {
      errorElement.innerText = errorMessage;
      getParent(inputElement, options.formGroupSelector).classList.add(
        'invalid'
      );
    } else {
      errorElement.innerText = '';
      getParent(inputElement, options.formGroupSelector).classList.remove(
        'invalid'
      );
    }
    return !errorMessage;
  }
  //take element of form
  const formElement = document.querySelector(options.form);
  if (formElement) {
    formElement.onsubmit = function (e) {
      //remove preventDefault
      e.preventDefault();
      var isFormValid = true;
      options.rules.forEach(function (rule) {
        var inputElement = formElement.querySelector(rule.selector);
        var isValid = validate(inputElement, rule);
        if (!isValid) {
          isFormValid = false;
        }
      });

      if (isFormValid) {
        if (typeof options.onSubmit === 'function') {
          var enableInput = formElement.querySelectorAll('[name]');
          var formValues = Array.from(enableInput).reduce(function (
            values,
            input
          ) {
            switch (input.type) {
              case 'radio':
                // if (input.matches(':checked')) {
                //   values[input.name] = input.value;
                // } else {
                //   values[input.name] = '';
                // }
                values[input.name] = formElement.querySelector(
                  'input[name = "' + input.name + '"]:checked'
                ).value;
                break;
              case 'checkbox':
                // Không checked thì cho value là một mảng
                if (!values[input.name]) values[input.name] = [];
                // Nếu checked thì push vào mảng
                if (input.checked) values[input.name].push(input.value);
                // Kiểm tra nếu là mảng rỗng thì gán là chuỗi ''
                if (values[input.name].length === 0) values[input.name] = '';
                break;
                // if (!input.matches(':checked')) {
                //   values[input.name] = '';
                //   return values;
                // }
                // if (!Array.isArray(values[input.name])) {
                //   values[input.name] = [];
                // }
                // values[input.name].push(input.value);
                //   if (!input.matches(':checked')) {
                //     values[input.name] = "";
                //     return values;
                //   }
                //   if (!Array.isArray(values[input.name])) {
                //     values[input.name] = [];
                //   }
                //   values[input.name].push(input.value);
                //   break;
                // case 'file':
                //   values[input.name] = input.files;
                break;
              default:
                values[input.name] = input.value;
            }
            return values;
          },
          {});
          options.onSubmit(formValues);
        }
      }
    };
    //save all rules for input
    options.rules.forEach(function (rule) {
      if (Array.isArray(selectorRule[rule.selector])) {
        selectorRule[rule.selector].push(rule.test);
      } else {
        selectorRule[rule.selector] = [rule.test];
      }
      var inputElements = formElement.querySelectorAll(rule.selector);
      Array.from(inputElements).forEach(function (inputElement) {
        inputElement.onblur = function () {
          //value: inputElement.value
          //test function: rule.test
          validate(inputElement, rule);
        };
        inputElement.oninput = function () {
          var errorElement = getParent(
            inputElement,
            options.formGroupSelector
          ).querySelector('.form-message');
          errorElement.innerText = '';
          getParent(inputElement, options.formGroupSelector).classList.remove(
            'invalid'
          );
        };
      });
    });
    //console.log(selectorRule);
  }
}
//define rules
//Principle Rules:
//1.when have bug => return bug
//2. when legal => not return
Validator.isRequired = function (selector, message) {
  return {
    selector: selector,
    test: function (value) {
      return value ? undefined : message || 'Vui lòng nhập trường này của bạn'; //.trim(): remove space
    },
  };
};

Validator.isEmail = function (selector, message) {
  return {
    selector: selector,
    test: function (value) {
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return regex.test(value) ? undefined : message || 'Vui lòng nhập Email';
    },
  };
};
Validator.minLength = function (selector, min, message) {
  return {
    selector: selector,
    test: function (value) {
      return value.length >= min
        ? undefined
        : message || `vui lòng nhập mật khẩu trên ${min} kí tự`;
    },
  };
};
Validator.isConfirmed = function (selector, getConfirmValue, message) {
  return {
    selector: selector,
    test: function (value) {
      return value === getConfirmValue()
        ? undefined
        : message || 'Dữ liệu nhập vào chưa chính xác';
    },
  };
};
